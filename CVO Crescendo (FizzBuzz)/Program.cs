﻿using System;

namespace CVO_Crescendo__FizzBuzz_
{
    public class Program
    {
        
        private static void Main()
        {

            //define constant vars
            const int devadebel1 = 3;
            const string devadebel1Str = "CVO";
            const int devadebel2 = 5;
            const string devadebel2Str = "Crescendo";
            const int startNumber = 1;
            const int endNumber = 100;

            for (var i = startNumber; i <= endNumber; i++)
            {

                //test if the nuber is devideble by the first var & print "CVO" to console.
                if (i % devadebel1 == 0) Console.Write(devadebel1Str) ;
                
                //test if the nuber is devideble by the second var & print "Crescendo" to console.
                if (i % devadebel2 == 0) Console.Write(devadebel2Str);
                
                //test if the number isn't devideble by ether number & print the number. 
                if (!(i % devadebel1 == 0 || i % devadebel2 == 0)) Console.Write(i);

                //write a space after eatch number.
                Console.Write(' ');

            }

            //stop program from ending.
            Console.ReadKey();

        }
    }
}
